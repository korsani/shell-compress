# shell-compress

Compress script commands, so that it's not that easily readable.

	$ echo 'PASSWORD=love' | shell-compress -b | tee p.sh
	eval "$(openssl base64 -d<<EOF|gzip -dc
	H4sICNvGtmMAA3RtcC5BZGZWNWpWUUM5AAtwDA4O9w9ysc3JL0vlAgDvNvbuDgAA
	AA==
	EOF
	)"
	🎈 gzip ~> 780% (14..117)
	$ cat p.sh
	#!/usr/bin/env bash
	eval "$(openssl base64 -d<<EOF|gzip -dc
	H4sICNvGtmMAA3RtcC5BZGZWNWpWUUM5AAtwDA4O9w9ysc3JL0vlAgDvNvbuDgAA
	AA==
	EOF
	)"
	echo "Password is $PASSWORD"
	$ bash p.sh
	Password is love
	$ 

Or:

	$ shell-compress -o /tmp/prsync < prsync
	🎈 gzip ~> 84% (1173..992)
	$ cat /tmp/prsync
	$ #!/usr/bin/env sh
	echo '🎈';t="$(mktemp sc-XXXXXXXX)";cat<<EOF|openssl base64 -d|gzip -dc>"$t";/usr/bin/env bash "$t" $@;rm -f "$t"
	H4sICETItmMAA3RtcC5LU0hJV0RrbnRmAI1T0W7aMBR9tr/iLoCAbmlGH1eKtDEm
	VWo7BN3DtFYoOA6xSOwodqAM2Lfv2ikBHjaVhxBfn5xzj+9x411Q6iKYCxlwuYJ5
	qBPagHGhFgXXGgq9kewDlFrIBeQrqlVZMA7um1SxMA2EZGkZ8SAV8yUWLvH7A4gb
	FjCVqkLTTEX8pu3Y2vR+dOM1OyjFZZhx8Jofva5HIzVz+50ubClx5BAXKkNszzsU
	jMLlFS45SxT4EryhKqXB7rDG7Kuldjzgrwojv0JzMv35MJx9Hz9OUcoyevhvlAc7
	WCMqRW2SF0KaGLynohVBLFKuUQqYyjdP0sIdNeJq5tTk/6fOV0gNvq4/hkEQ8VUg
	yzSle+vWhMXbvNa+ohIJ57XUDsL1Etpb1zw0e/v2qZWW/nLuoWPnM5tfLbIlVD1Z
	OHYBPovBB39YM1++GjjpfgcO+TKsHKKFUocLXjlw0/Ca9yP4BX4GfTvuATzble+O
	TOVGQ786PXwXSmq737dyA+gbNXid6YHLz8gnp6jKKoQXB8Ae8/nnDT+6TnCOtgVM
	CR4ryl1DhAIMg1dVhKSE+EmXEOcF9/mLMHB9bcsZll1s7RBwSyciNtXW0RNijhl4
	76CnyIsuPoiIbRe/69O1jZiEW3FCTsZOCE81d1U3+6oWC3xaNq5DRokjx/hITo+8
	NnLPsNv9S+fMXo8ipcsCSzhbPnAe8eiby3xWamOveaOOXCxkVBP6ZpNziE8uziFs
	ze3n6fD2djb+MRnfjfYNaCEZHt6hPhlNR4/7Koe27lG+wojjLXDL89tD/wLAYONy
	lQQAAA==
	EOF
	$ bash /tmp/prsync
	🎈
	sc.Zpc2 [ -m <mode> ] [ --rsyncopts <rsync options> ] <from> <to>

	-m      : tar ou rsync*

	$

It can compress not only shell file, but also any shebanged script (bc(1), perl(1), ...)

# Starting

Put this script somewhere accessible, then ```chmod +x``` it.

# Running

## Usage

	shell-compress [ -d ] [ -b ] [ -o <file> ] [ -p <password> ] < <script commands>
	shell-compress [ -d ] [ -b ] [ -o <file> ] [ -p <password> ] -i <script commands file>

	shell-compress [ -d ] -u < <shell-compressed-stuff>

	-b : find best compression algorithm
	-d : debug
	-i : input file to use, instead of stdin
	-o : output to that file instead of stdout
	-p : encrypt with password
	-u : uncompress stuff compressed by shell-compress

## Examples

### Basic : a shell file

	$ shell-compress -b -i shell-compress -p compressme > /tmp/sc
	🎈 xz ~> 55% (7561..4175)
	$ bash /tmp/sc -h
	🎈
	enter aes-256-cbc decryption password:

	Compress (obfuscate?) script commands

        shell-compress [ -d ] [ -b ] [ -o <file> ] [ -p <password> ] < script_commands
        shell-compress [ -d ] [ -b ] [ -o <file> ] [ -p <password> ] -i script_commands_file
        shell-compress [ -d ] -u < file
        shell-compress [ -d ] -i file
		...

### An other script file

	$ shell-compress -b -i perl-script.pl -o /tmp/compressed.sh
	$ bash /tmp/compressed.sh

### Colors declaration in a shell file

	$ cat /etc/colors
	# ASCII colors vars
	# https://www.clubnix.fr/book/export/html/319
	# Regular
	declare -A COLOR
	COLOR[BLACK]='\e[30m'
	COLOR[RED]='\e[31m'
	COLOR[GREEN]='\e[32m'
	COLOR[BROWN]='\e[33m'
	COLOR[BLUE]='\e[34m'
	COLOR[PURPLE]='\e[35m'
	COLOR[CYAN]='\e[36m'
	COLOR[WHITE]='\e[37m'
	# Bold
	COLOR[BLD_BLACK]='\e[1;30m'
	COLOR[BLD_RED]='\e[1;31m'
	COLOR[BLD_GREEN]='\e[1;32m'
	COLOR[BLD_YELLOW]='\e[1;33m'
	COLOR[BLD_BLUE]='\e[1;34m'
	COLOR[BLD_PURPLE]='\e[1;35m'
	COLOR[BLD_CYAN]='\e[1;36m'
	COLOR[BLD_WHITE]='\e[1;37m'
	# Background only
	COLOR[BG_BLACK]='\e[40m'
	COLOR[BG_RED]='\e[41m'
	COLOR[BG_GREEN]='\e[42m'
	COLOR[BG_YELLOW]='\e[43m'
	COLOR[BG_BLUE]='\e[44m'
	COLOR[BG_PURPLE]='\e[45m'
	COLOR[BG_CYAN]='\e[46m'
	COLOR[BG_WHITE]='\e[47m'
	# Underlined
	COLOR[U_BLACK]='\e[4;30m'
	COLOR[U_RED]='\e[4;31m'
	COLOR[U_GREEN]='\e[4;32m'
	COLOR[U_YELLOW]='\e[4;33m'
	COLOR[U_BLUE]='\e[4;34m'
	COLOR[U_PURPLE]='\e[4;35m'
	COLOR[U_CYAN]='\e[4;36m'
	COLOR[U_WHITE]='\e[4;37m'
	COLOR[RESET]='\e[0m'
	COLOR[BLINK]='\e[5m'
	COLOR[VIDEO]='\e[7m'

	$ shell-compress -i /etc/colors | tee script.sh
	eval "$(openssl base64 -d<<EOF|gzip -dc
	H4sICGPYSWEAA3RtcC5CVjE0VTdPTUdJAFWTQW+CMBxH736KJh48TYZUl23ZAbFx
	ZEQM2hnjlkWhUyNSU3G4b786aPvvRZP3KunLD9vInwVhiFKec3FGP2txbrXRrixP
	5yfHqaqqm+aXTbG/dr+Fs+H84LDriYvS2ZXH3PHcR3k6YdtLvhatjKXyi6E7HwVx
	FCet/8/VMPKDt8+XzgdbeffHTkMTMmqYq9k4IWTS0J6mwyReKOoZGlHSQKzhlCbT
	SOG+xsHSVw8YaLh4Defq6IOkbTTkeaafPvoC93afwc1vTt9eGtcyoEG6nuWWJIri
	hZaeJU2QVNhSMEvKviVNnFQDS4FE6ZrIdXrYCn4pMsSL/FedHsNcDGLHphW7EINQ
	3IMCVmIPGpOIMeSwD/ehMXF4ADkow3UXLTIm8n3B1ITUKoIDUpAE56NWExyP2lFw
	Ogqr4HDUzoKzUdgFR6NWWD2Z+rvMyLzm8E0MJ02gefh7OCJxDW8//wNwfuhs4QMA
	AA==
	EOF
	)"
	🎈 gzip ~> 57% (993..572)
	$ cat a.sh
	#!/usr/bin/env bash
	# Declare some colors
	eval "$(openssl base64 -d<<EOF|gzip -dc
	H4sICBTOtmMAA3RtcC5WcWk4RDhiNlFvAFWTQW+CMBxH736KJh48TYZUl23ZAbFx
	ZEQM2hnjlkWhUyNSU3G4b786aPvvRZP3KunLD9vInwVhiFKec3FGP2txbrXRrixP
	5yfHqaqqm+aXTbG/dr+Fs+H84LDriYvS2ZXH3PHcR3k6YdtLvhatjKXyi6E7HwVx
	FCet/8/VMPKDt8+XzgdbeffHTkMTMmqYq9k4IWTS0J6mwyReKOoZGlHSQKzhlCbT
	SOG+xsHSVw8YaLh4Defq6IOkbTTkeaafPvoC93afwc1vTt9eGtcyoEG6nuWWJIri
	hZaeJU2QVNhSMEvKviVNnFQDS4FE6ZrIdXrYCn4pMsSL/FedHsNcDGLHphW7EINQ
	3IMCVmIPGpOIMeSwD/ehMXF4ADkow3UXLTIm8n3B1ITUKoIDUpAE56NWExyP2lFw
	Ogqr4HDUzoKzUdgFR6NWWD2Z+rvMyLzm8E0MJ02gefh7OCJxDW8//wNwfuhs4QMA
	AA==
	EOF
	)"
	echo -e "${COLOR[BLD_RED]}This is red${COLOR[RESET]}"
	$ bash a.sh
	This is red


# Made with

- fun
